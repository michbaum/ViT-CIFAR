import argparse

import comet_ml
import torch
import torch.nn as nn
import torch.nn.functional as F
import torchvision
import pytorch_lightning as pl
import warmup_scheduler
import numpy as np
from lightning.pytorch.accelerators import find_usable_cuda_devices
from pytorch_lightning.loggers import WandbLogger
import wandb

from utils import get_model, get_dataset, get_experiment_name, get_criterion, get_parent_name
from da import CutMix, MixUp

parser = argparse.ArgumentParser()
parser.add_argument("--api-key", help="API Key for Comet.ml")
parser.add_argument("--dataset", default="c10", type=str, help="[c10, c100, svhn]")
parser.add_argument("--num-classes", default=10, type=int)
parser.add_argument("--model-name", default="vit", help="[vit]", type=str)
parser.add_argument("--patch", default=8, type=int)
parser.add_argument("--batch-size", default=128, type=int)
parser.add_argument("--eval-batch-size", default=1024, type=int)
parser.add_argument("--lr", default=1e-3, type=float)
parser.add_argument("--min-lr", default=1e-5, type=float)
parser.add_argument("--beta1", default=0.9, type=float)
parser.add_argument("--beta2", default=0.999, type=float)
parser.add_argument("--off-benchmark", action="store_true")
parser.add_argument("--max-epochs", default=200, type=int)
parser.add_argument("--dry-run", action="store_true")
parser.add_argument("--weight-decay", default=5e-5, type=float)
parser.add_argument("--warmup-epoch", default=5, type=int)
parser.add_argument("--precision", default=16, type=int)
parser.add_argument("--autoaugment", action="store_true")
parser.add_argument("--criterion", default="ce")
parser.add_argument("--label-smoothing", action="store_true")
parser.add_argument("--smoothing", default=0.1, type=float)
parser.add_argument("--rcpaste", action="store_true")
parser.add_argument("--cutmix", action="store_true")
parser.add_argument("--mixup", action="store_true")
parser.add_argument("--dropout", default=0.0, type=float)
parser.add_argument("--head", default=12, type=int)
parser.add_argument("--num-layers", default=7, type=int)
parser.add_argument("--hidden", default=384, type=int)
parser.add_argument("--mlp-hidden", default=384, type=int)
parser.add_argument("--off-cls-token", action="store_true")
parser.add_argument("--seed", default=42, type=int)
parser.add_argument("--project-name", default="VisionTransformer")
# Add flags to train a model based on a parent model, sharing the embedding layer
parser.add_argument("--parent-model-seed", default=None, type=int)
parser.add_argument("--parent-copy-all-weights", action="store_true")
# Add more flags for model inspection after training
parser.add_argument("--inspection-only", action="store_true")
parser.add_argument("--print-test-acc", action="store_true")
# Add a flag to introduce a sanity check making sure that the embedding, pos_embedding and cls_token are the same after training
# Compares the model with seed "seed" with the parent model with seed "parent-model-seed"
# Requires the model with seed "seed" to be already trained and saved
parser.add_argument("--sanity-check", action="store_true")
parser.add_argument("--no-res", action="store_true", default=False)

args = parser.parse_args()
torch.manual_seed(args.seed)
np.random.seed(args.seed)
args.benchmark = True if not args.off_benchmark else False
args.gpus = torch.cuda.device_count()
args.num_workers = 4*args.gpus if args.gpus else 8
args.is_cls_token = True if not args.off_cls_token else False
if not args.gpus:
    args.precision=32

if args.mlp_hidden != args.hidden*4:
    print(f"[INFO] In original paper, mlp_hidden(CURRENT:{args.mlp_hidden}) is set to: {args.hidden*4}(={args.hidden}*4)")

train_ds, test_ds = get_dataset(args)
train_dl = torch.utils.data.DataLoader(train_ds, batch_size=args.batch_size, shuffle=True, num_workers=args.num_workers, pin_memory=True)
val_dl = torch.utils.data.DataLoader(test_ds, batch_size=args.eval_batch_size, num_workers=args.num_workers, pin_memory=True)
test_dl = torch.utils.data.DataLoader(test_ds, batch_size=1, num_workers=args.num_workers, pin_memory=True)

class Net(pl.LightningModule):
    def __init__(self, hparams):
        super(Net, self).__init__()
        # self.hparams = hparams
        self.hparams.update(vars(hparams))
        self.model = get_model(hparams)
        self.criterion = get_criterion(args)
        if hparams.cutmix:
            self.cutmix = CutMix(hparams.size, beta=1.)
        if hparams.mixup:
            self.mixup = MixUp(alpha=1.)
        self.log_image_flag = hparams.api_key is None

    def forward(self, x):
        return self.model(x)

    def configure_optimizers(self):
        self.optimizer = torch.optim.Adam(self.model.parameters(), lr=self.hparams.lr, betas=(self.hparams.beta1, self.hparams.beta2), weight_decay=self.hparams.weight_decay)
        self.base_scheduler = torch.optim.lr_scheduler.CosineAnnealingLR(self.optimizer, T_max=self.hparams.max_epochs, eta_min=self.hparams.min_lr)
        self.scheduler = warmup_scheduler.GradualWarmupScheduler(self.optimizer, multiplier=1., total_epoch=self.hparams.warmup_epoch, after_scheduler=self.base_scheduler)
        return [self.optimizer], [self.scheduler]

    def training_step(self, batch, batch_idx):
        img, label = batch
        if self.hparams.cutmix or self.hparams.mixup:
            if self.hparams.cutmix:
                img, label, rand_label, lambda_= self.cutmix((img, label))
            elif self.hparams.mixup:
                if np.random.rand() <= 0.8:
                    img, label, rand_label, lambda_ = self.mixup((img, label))
                else:
                    img, label, rand_label, lambda_ = img, label, torch.zeros_like(label), 1.
            out = self.model(img)
            loss = self.criterion(out, label)*lambda_ + self.criterion(out, rand_label)*(1.-lambda_)
        else:
            out = self(img)
            loss = self.criterion(out, label)

        if not self.log_image_flag and not self.hparams.dry_run:
            self.log_image_flag = True
            self._log_image(img.clone().detach().cpu())

        acc = torch.eq(out.argmax(-1), label).float().mean()
        self.log("loss", loss)
        self.log("acc", acc)
        return loss

    def training_epoch_end(self, outputs):
        self.log("lr", self.optimizer.param_groups[0]["lr"], on_epoch=self.current_epoch)

    def validation_step(self, batch, batch_idx):
        img, label = batch
        out = self(img)
        loss = self.criterion(out, label)
        acc = torch.eq(out.argmax(-1), label).float().mean()
        self.log("val_loss", loss)
        self.log("val_acc", acc)
        return loss
    
    def test_step(self, batch, batch_idx):
        img, label = batch
        out = self(img)
        loss = self.criterion(out, label)
        acc = torch.eq(out.argmax(-1), label).float().mean()
        self.log("test_loss", loss)
        self.log("test_acc", acc)
        return loss

    def _log_image(self, image):
        grid = torchvision.utils.make_grid(image, nrow=4)
        self.logger.experiment.log_image(grid.permute(1,2,0))
        print("[INFO] LOG IMAGE!!!")


if __name__ == "__main__":
    print("CUDA:", torch.cuda.is_available())
    experiment_name = get_experiment_name(args)
    print(experiment_name)
    wandb.init()
    wandb_logger = WandbLogger()
    # if args.api_key:
    #     print("[INFO] Log with Comet.ml!")
    #     logger = pl.loggers.CometLogger(
    #         api_key=args.api_key,
    #         save_dir="logs",
    #         project_name=args.project_name,
    #         experiment_name=experiment_name
    #     )
    #     refresh_rate = 0
    # else:
    #     print("[INFO] Log with CSV")
    #     logger = pl.loggers.CSVLogger(
    #         save_dir="logs",
    #         name=experiment_name
    #     )
    #     refresh_rate = 1
    net = Net(args)

    # Check if the model should be derived from a pretrained model
    # This is only supported if the model have the same structure
    if args.parent_model_seed:
        parent_name = get_parent_name(args)
        print(f"[INFO] Load weights from {parent_name}.")
        state_dict_parent = torch.load(f"weights/{parent_name}.pth")
        if args.parent_copy_all_weights:
            net.load_state_dict(state_dict_parent)
        else:
            # Only load the embedding layer with the positional embedding and cls token, leave the rest initialized with the given seed
            # Make sure that the loaded parameters are kept fix
            net.model.emb.weight = nn.parameter.Parameter(state_dict_parent["model.emb.weight"], requires_grad=False)
            #net.model.emb.bias = nn.parameter.Parameter(state_dict_parent["model.emb.bias"], requires_grad=False)
            net.model.pos_emb = nn.parameter.Parameter(state_dict_parent["model.pos_emb"], requires_grad=False)
            net.model.cls_token = nn.parameter.Parameter(state_dict_parent["model.cls_token"], requires_grad=False)
        
        if args.sanity_check:
            print(f"[INFO] Sanity check model with seed {args.parent_model_seed} and seed {args.seed}!")
            state_dict_child = torch.load(f"weights/{experiment_name}.pth")
            embedding_weight_parent = nn.parameter.Parameter(state_dict_parent["model.emb.weight"])
            embedding_weight_child = nn.parameter.Parameter(state_dict_child["model.emb.weight"])
            if torch.all(torch.eq(embedding_weight_parent, embedding_weight_child)):
                print("[INFO] Embedding layer weights agree!")
            else:
                print("[INFO] Embedding layer weights do not agree!")
                print(embedding_weight_parent)
                print(embedding_weight_child)
                raise ValueError("Embedding layer weights do not agree!")
            embedding_bias_parent = nn.parameter.Parameter(state_dict_parent["model.emb.bias"])
            embedding_bias_child = nn.parameter.Parameter(state_dict_child["model.emb.bias"])
            if torch.all(torch.eq(embedding_bias_parent, embedding_bias_child)):
                print("[INFO] Embedding layer bias agree!")
            else:
                print("[INFO] Embedding layer bias do not agree!")
                print(embedding_bias_parent)
                print(embedding_bias_child)
                raise ValueError("Embedding layer bias do not agree!")
            pos_emb_parent = nn.parameter.Parameter(state_dict_parent["model.pos_emb"])
            pos_emb_child = nn.parameter.Parameter(state_dict_child["model.pos_emb"])
            if torch.all(torch.eq(pos_emb_parent, pos_emb_child)):
                print("[INFO] Positional embedding agree!")
            else:
                print("[INFO] Positional embedding do not agree!")
                print(pos_emb_parent)
                print(pos_emb_child)
                raise ValueError("Positional embedding do not agree!")
            cls_token_parent = nn.parameter.Parameter(state_dict_parent["model.cls_token"])
            cls_token_child = nn.parameter.Parameter(state_dict_child["model.cls_token"])
            if torch.all(torch.eq(cls_token_parent, cls_token_child)):
                print("[INFO] CLS token agree!")
            else:
                print("[INFO] CLS token do not agree!")
                print(cls_token_parent)
                print(cls_token_child)
                raise ValueError("CLS token do not agree!")
            print("[INFO] Sanity check passed!")

            first_enc_q_parent = nn.parameter.Parameter(state_dict_parent["model.enc.0.msa.q.weight"])
            first_enc_q_child = nn.parameter.Parameter(state_dict_child["model.enc.0.msa.q.weight"])
            if torch.all(torch.eq(first_enc_q_parent, first_enc_q_child)):
                print("[INFO] First encoder query agree! BAD")
                raise ValueError("First encoder query agree!")
            first_enc_k_parent = nn.parameter.Parameter(state_dict_parent["model.enc.0.msa.k.weight"])
            first_enc_k_child = nn.parameter.Parameter(state_dict_child["model.enc.0.msa.k.weight"])
            if torch.all(torch.eq(first_enc_k_parent, first_enc_k_child)):
                print("[INFO] First encoder key agree! BAD")
                raise ValueError("First encoder key agree!")
            first_enc_v_parent = nn.parameter.Parameter(state_dict_parent["model.enc.0.msa.v.weight"])
            first_enc_v_child = nn.parameter.Parameter(state_dict_child["model.enc.0.msa.v.weight"])
            if torch.all(torch.eq(first_enc_v_parent, first_enc_v_child)):
                print("[INFO] First encoder value agree! BAD")
                raise ValueError("First encoder value agree!")
            first_enc_out_parent = nn.parameter.Parameter(state_dict_parent["model.enc.0.msa.o.weight"])
            first_enc_out_child = nn.parameter.Parameter(state_dict_child["model.enc.0.msa.o.weight"])
            if torch.all(torch.eq(first_enc_out_parent, first_enc_out_child)):
                print("[INFO] First encoder output layer agree! BAD")
                raise ValueError("First encoder output layer agree!")

    trainer = pl.Trainer(logger=wandb_logger, precision=args.precision,fast_dev_run=args.dry_run, benchmark=args.benchmark, gpus=args.gpus, weights_summary="full", max_epochs=args.max_epochs)
    if args.inspection_only:
        print(net.model)
        for name, param in net.state_dict().items():
            print(name)
    else:
        for name, layer in net.named_modules():
            if isinstance(layer, nn.Linear):
                layer.bias = None
        trainer.fit(model=net, train_dataloader=train_dl, val_dataloaders=val_dl)
        if not args.dry_run:
            print("access.\n")
            model_path = f"weights/{experiment_name}.pth"
            torch.save(net.state_dict(), model_path)
            if args.api_key:
                logger.experiment.log_asset(file_name=experiment_name, file_data=model_path)

    if args.print_test_acc:
        net.eval()
        trainer.test(model=net, test_dataloaders=test_dl)

    wandb.finish()





